FROM python:3-alpine

# Create app directory
WORKDIR /app

# Copy requirements.txt into container
COPY requirements.txt.docker ./

# Install app dependencies
RUN pip install -r requirements.txt.docker

# Bundle app source
COPY . .

CMD [ "python", "szreader.py", "-n", "-c", "szreader.config" ]
