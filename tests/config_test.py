from unittest import TestCase
from management.Configuration import Configuration
from tests import TEST_CONFIG


class Config(TestCase):
    """ TestCase for configuration"""
    def test_fail_config(self):
        """ Test successful if configuration could not be read since no file was supplied"""
        with self.assertRaises(Exception):
            config = Configuration()

    def test_read_config(self):
        """ Test successful if supplied configfile could be read"""
        config = Configuration(TEST_CONFIG)
