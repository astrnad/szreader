from unittest import TestCase
from management import Decoder


class DecoderTest(TestCase):
    """Unittest Class for testing decoding."""

    def setUp(self):
        """Setup default values for testing."""

        self.txt = "7E A0 77 CF 02 23 13 BB 45 E6 E7 00 DB 08 49 53 4B 69 74 55 0B CD 5F 20 00 00 0D 0B 00 A6 6D E2 " \
                   "E6 7C 6F 06 23 06 1E 55 11 E7 27 E9 62 0A 52 63 B5 C7 88 C5 E3 FE 5C C0 54 38 B3 E6 D9 5C 45 08 " \
                   "8B 38 58 8A 11 C3 09 85 80 A8 43 04 A2 09 C3 27 86 3D 99 2F 71 55 12 0D E9 F2 21 20 29 DC C6 8C " \
                   "76 4A A0 5E 5B 0C 31 E9 D4 E3 2A 22 4C 1B 53 71 8B 92 C5 DA 47 B9 38 69 7E "
        self.key = "4256 EAA3 DD16 5139 2895 62E8 8F2C 6565"

    def test_decode_txt(self):
        """Establishes baseline for decoding from known text and key."""

        dec = Decoder.decode(self.key, self.txt)
        self.assertTrue(dec.startswith("<HDLC"))
