[default]

# Define modules to be used as a datasource
# Possible values: serial, textin, mqttin
# If multiple sources of the same type are configured, append quantifier, e.g.: text1,text2,serial - No spaces
# All datasources need their respective "decryptionkey" setting defined
sourceModules = serial

# Define modules to be used as a datasink
# Possible values: http, mqttout, textout
# If multiple sinks of the same type are configured, append quantifier, e.g.: http1, http2, mqtt
sinkModules = http1,http2


[serial]

# Serial settings
port = /dev/ttyUSB0
baudrate = 115200

# Decryption key to use
decryptionkey =


[textin1]

# File to read
inputFile = examples/PowerConsume.txt

# Interval between lines to feed in seconds
interval = 1

# Decryption key to use
decryptionkey =


[textin2]

# File to read
inputFile = examples/PowerDeliver.txt

# Interval between lines to feed in seconds
interval = 1

# Decryption key to use
decryptionkey =


[mqttin]

# MQTT settings
host = 10.0.0.4
port = 1883
qos = 1
clientname = szreaderin
username =
password =

# MQTT Last Will
wtopic = szreader/readeroff
wpayload = SZReader Data Reader unexpectedly disconnected!
wqos = 0
wretain = 0

# Topic to subscribe to for encrypted DLMS packets
topic = szreader/rawMessage

# Decryption key to use
decryptionkey =


[mqttout]

# MQTT settings
host = 10.0.0.4
port = 1883
qos = 0
retain = 0
clientname = szreaderout
username =
password =

# MQTT Last Will
wtopic = szreader/writeroff
wpayload = SZReader Data Writer unexpectedly disconnected!
wqos = 0
wretain = 0

# Dictionary of topics to publish data to, jinja templates used for values
# Key: topic to publish to
# Value: value for topic
# Uncomment the topics, you want to enable. Add comma (,) at the end of each line, except the last one.
topics = {
#    "szreader/sourcename": "{{ sourcename }}",
#    "szreader/timestamp": "{{ timestamp }}",
#    "szreader/current_counter_in": "{{ current_counter_in }}",
#    "szreader/current_counter_out": "{{ current_counter_out }}",
#    "szreader/current_power_in": "{{ current_power_in }}",
#    "szreader/current_power_out": "{{ current_power_out }}",
#    "szreader/wholeMessage": "timestamp:{{ timestamp }};in:{{ current_counter_in }};out:{{ current_counter_out }}"
    }


[http1]

# Sends data to defined http(s) Host, using templates
# Following template fields are available and will be filled with data:
# - current_power_in (Current Power consumed)
# - current_power_out (Current power sent to grid)
# - timestamp (Unix Timestamp of power reading in seconds)

# host url to send data to, can be template

# working volkszaehler-config, connecting directly to the PMP-Server
# {{timestamp}} needs to be multiplied by 1000 since volkszaehler only accepts milliseconds
#host = http://<fqdn>:8080/data/8c1a48e0-6f8c-11ea-a9bf-c9022cc25712.json?{{ timestamp *1000 }}&value={{ current_power_in }}

# working volkszaehler-config, connecting via Proxy
# {{timestamp}} needs to be multiplied by 1000 since volkszaehler only accepts milliseconds
host = http://<fqdn>/middleware.php/data/8c1a48e0-6f8c-11ea-a9bf-c9022cc25712.json?{{ timestamp *1000 }}&value={{ current_power_in }}

# HTTP Method to use to send data
method = POST


[textout]

# File for output
path = datasink.txt
