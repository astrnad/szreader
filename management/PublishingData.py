from datetime import datetime


class PublishingData:
    def __init__(self, sourcename=None, current_counter_in=None, current_counter_out=None, current_power_in=None, current_power_out=None, timestamp=datetime.now()):
        """
        Args:
            sourcename: Name of the configured Datasource
            current_counter_in: Current counter reading consumed
            current_counter_out: Current counter reading sent to grid
            current_power_in: Current power consumed
            current_power_out: Current power sent to grid
            timestamp: Time and date of data
        """
        self._sourcename = sourcename
        self._current_counter_in = current_counter_in
        self._current_counter_out = current_counter_out
        self._current_power_in = current_power_in
        self._current_power_out = current_power_out
        self._timestamp = timestamp

    @property
    def current_counter_in(self) -> float:
        return self._current_counter_in

    @current_counter_in.setter
    def current_counter_in(self, value: float):
        """
        Args:
            value (float): Current counter reading consumed
        """
        self._current_counter_in = value

    @property
    def current_counter_out(self) -> float:
        return self._current_counter_out

    @current_counter_out.setter
    def current_counter_out(self, value: float):
        """
        Args:
            value (float): Current counter reading sent to grid
        """
        self._current_counter_out = value

    @property
    def current_power_in(self) -> float:
        return self._current_power_in

    @current_power_in.setter
    def current_power_in(self, value: float):
        """
        Args:
            value (float): Current Power consumed
        """
        self._current_power_in = value

    @property
    def current_power_out(self) -> float:
        return self._current_power_out

    @current_power_out.setter
    def current_power_out(self, value: float):
        """
        Args:
            value (float): Current power sent to grid
        """
        self._current_power_out = value

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        """
        Args:
            value: Timestamp of data
        """
        self._timestamp = value

    @property
    def sourcename(self):
        return self._sourcename

    def __repr__(self) -> str:
        return "{}: {}: Counter in: {} Wh, Counter out: {} Wh, In: {} W, Out: {} W".format(self._sourcename, self._timestamp, self._current_counter_in, self.current_counter_out, self._current_power_in, self._current_power_out)
