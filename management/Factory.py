from datasink import SINK_MODULE_HTTP, SINK_MODULE_TEXTOUT, SINK_MODULE_MQTTOUT
from datasink.HttpDataSink import HttpDataSink
from datasink.IDataSink import IDataSink
from datasink.MqttDataSink import MqttDataSink
from datasink.TextDataSink import TextDataSink
from datasource import SOURCE_MODULE_SERIAL, SOURCE_MODULE_TEXTIN, SOURCE_MODULE_MQTTIN
from datasource.IDataSource import IDataSource
from datasource.MqttDataSource import MqttDataSource
from datasource.SerialDataSource import SerialDataSource
from datasource.TextFileDataSource import TextFileDataSource
from management.Configuration import Configuration
import logging

logger = logging.getLogger(__name__)


class Factory:
    def __init__(self, configuration: Configuration):
        self._configuration = configuration
        logger.debug("Initialized Factory")

    def create_sink(self, name: str) -> IDataSink:
        logger.debug("Creating DataSink for name {}".format(name))
        if self._configuration.configuration.has_section(name):
            if SINK_MODULE_HTTP in name:
                return HttpDataSink(self._configuration[name])
            elif SINK_MODULE_TEXTOUT in name:
                return TextDataSink(self._configuration[name])
            elif SINK_MODULE_MQTTOUT in name:
                return MqttDataSink(self._configuration[name])
            else:
                raise NotImplementedError("DataSink for name '{}' not implemented".format(name))

        else:
            raise NotImplementedError("No configuration present for DataSink with name '{}'".format(name))

    def create_source(self, name: str, callback) -> IDataSource:
        logger.debug("Creating DataSource for name {}".format(name))
        if self._configuration.configuration.has_section(name):
            if SOURCE_MODULE_SERIAL in name:
                return SerialDataSource(self._configuration[name], callback)
            elif SOURCE_MODULE_TEXTIN in name:
                return TextFileDataSource(self._configuration[name], callback)
            elif SOURCE_MODULE_MQTTIN in name:
                return MqttDataSource(self._configuration[name], callback)
            else:
                raise NotImplementedError("DataSource for name '{}' not implemented".format(name))
        else:
            raise NotImplementedError("No configuration present for DataSource with name '{}'".format(name))

