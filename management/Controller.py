from datasource.IDataSource import IDataSource
from management import Decoder
from management.Configuration import Configuration
from management.Factory import Factory
from management.MessageMangler import MessageMangler
import logging
from threading import Lock

logger = logging.getLogger(__name__)


class Controller:
    def __init__(self, config_file_path=None):
        """
        Args:
            config_file_path: Path to configuration file
        """
        logger.info("Initializing Controller...")
        self._config = Configuration(config_file_path)
        self._factory = Factory(self._config)
        self._data_sources_threads = {}
        self._data_sinks = {}
        self._lock = Lock()
        default = self._config.configuration["default"]
        self._initialize_sources(default)
        self._initialize_sinks(default)
        logger.info("Controller initialized successfully with {} sources and {} sinks".format(len(self._data_sources_threads), len(self._data_sinks)))

    def _initialize_sources(self, default):
        """
        Args:
            default: DEFAULT section of configuration object
        """
        source_modules = default.get("sourceModules")
        if len(source_modules) < 1:
            raise LookupError("No source modules defined in configuration")
        source_modules = source_modules.split(',')
        logger.debug("Initializing sources: {}".format(repr(source_modules)))
        for source_module in source_modules:
            if source_module not in self._data_sources_threads.keys():
                self._data_sources_threads[source_module] = self._factory.create_source(source_module,
                                                                                        self._source_callback)

    def _initialize_sinks(self, default):
        """
        Args:
            default: DEFAULT section of configuration object
        """
        sink_modules = default.get("sinkModules")
        if len(sink_modules) < 1:
            raise LookupError("No sink modules defined in configuration")
        sink_modules = sink_modules.split(',')
        logger.debug("Initializing sinks: {}".format(repr(sink_modules)))
        for sink_module in sink_modules:
            if sink_module not in self._data_sinks.keys():
                self._data_sinks[sink_module] = self._factory.create_sink(sink_module)

    def start(self):
        logger.debug("Starting sources...")
        for key in self._data_sources_threads.keys():
            self._data_sources_threads[key].start()
        logger.debug("Sources started")

    def _source_callback(self, sender: IDataSource, data: str):
        """
        Args:
            sender (IDataSource): Datasource which sent the event with data

            data (str): Data coming from source, typically received coded
                message
        """
        self._lock.acquire()
        logger.debug("Got Data from source {}".format(sender.configurationname))
        dec = None
        mangler = None
        try:
            dec = Decoder.decode(sender.decryptionkey, data)
        except:
            logger.exception("Error decoding data")
        if dec is not None:
            try:
                mangler = MessageMangler(sender.configurationname, dec)
            except:
                logger.exception("Error mangling data")
        for sink in self._data_sinks.keys():
            if mangler is not None:
                if mangler.is_data_valid():
                    msg = mangler.get_publishing_data()
                    self._data_sinks[sink].send_data(msg)
                else:
                    logger.warning("Invalid message from source {}: {}".format(sender.configurationname, dec))
        self._lock.release()

    def any_source_running(self):
        running = False
        for source in self._data_sources_threads.keys():
            running = running or self._data_sources_threads[source].is_alive()
        return running

    def shutdown(self):
        for source in self._data_sources_threads.values():
            logger.debug("Signaling source {} to shutdown...".format(source.configurationname))
            source.shutdown_flag.set()
            source.join()
            logger.debug("Source {} cleanly shutdown".format(source.configurationname))

        for sink in self._data_sinks.values():
            sink.stop_sink()
