from abc import ABCMeta, abstractmethod

from management.PublishingData import PublishingData


class IDataSink:
    """Base Interface for publishing classes, e.g. MQTT or HTTP(S)"""
    __metaclass__ = ABCMeta

    def __init__(self, config):
        """
        Args:
            config: Object holding configuration to be used by the publisher
        """
        self._config = config

    @abstractmethod
    def send_data(self, data: PublishingData): raise NotImplementedError

    def stop_sink(self): pass